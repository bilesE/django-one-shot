from django.contrib import admin
from todos.models import TodoList, TodoItem


@admin.register(TodoList,)
# Register your models here.
class TodoListAdmin(admin.ModelAdmin):
    list_display = ('id', 'name')

# admin.site.register(TodoList, TodoListAdmin)

@admin.register(TodoItem)
class TodoItemAdmin(admin.ModelAdmin):
    list_display = ('task', 'due_date', 'is_completed')

# admin.site.register(TodoItem, TodoItemAdmin)
