from django.urls import path
from todos.views import todo_list_list, todo_list_detail, show_todos, todo_list_create

app_name = 'todos'

urlpatterns = [
    #register the view path from views.
    path("<int:id>/", todo_list_detail, name="todo_list_detail"),
    path("", todo_list_list, name="todo_list_list"),
    path("show_todos/", show_todos, name="show_todos"),
    path("create/", todo_list_create, name="todo_list_create"),
]
