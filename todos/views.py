from django.shortcuts import render, get_object_or_404, redirect
from todos.models import TodoList


# Create your views here.
def show_todos(request):
    todos = TodoList.objects.all()
    context = {
        "show_todos": todos,
    }
    return render(request, context)


#create list view for todolist
def todo_list_list(request):
    todo_lists = TodoList.objects.all()
    context = {
        "todo_list_list": todo_lists,
    }
    return render(request, "todos/lists.html", context)

#define a view that will feature the details of a todolist
def todo_list_detail(request, id):
    todo_item = get_object_or_404(TodoList, id=id)
    context = {
        "list_details": todo_item,
    }
    return render(request, 'todos/detail.html', context)

def todo_list_create(request):
    if request.method == "POST":
        form = TodoForm(request.POST)
        if form.is_valid():
            list = form.save()
            return redirect("todo_list_detail", id=list.id)
    else:
        form = TodoForm()
    context = {
        "form": form
    }
    return render(request, "todos/create.html", context)
