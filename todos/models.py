from django.db import models
from django.conf import settings

# Create your models here.
#write a class for todo?
class TodoList(models.Model):
    name = models.CharField(max_length=100)
    created_on = models.DateTimeField(null=True, blank=True,)

    def __str__(self):
        return self.name

class TodoItem(models.Model):
    task = models.CharField(max_length=100)
    due_date = models.DateTimeField(null=True, blank=True)
    is_completed = models.BooleanField(default=False)
    todolist = models.ForeignKey(
        TodoList,
        related_name="items",
        on_delete=models.CASCADE)
    
    def __str__(self):
        return self.task
